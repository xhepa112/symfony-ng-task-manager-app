<?php

namespace App\Controller;

use App\Service\JwtAuth;
use FOS\RestBundle\Controller\FOSRestController;
//use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/*use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;*/
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\View\View;

use App\Entity\Task;
use App\Entity\User;

use FOS\RestBundle\Controller\Annotations as Rest;

/**
 *
 * @Route("/api")
 */
class TaskController extends FOSRestController
{
    /**
     * Lists all Tasks.
     * @Rest\Post("/my-tasks")
     *
     * @return View
     */
    public function getAllTasks(Request $request,JwtAuth $jwt_auth)
    {
        $token = $request->get('authorization',null);

        $authCheck = $jwt_auth->checkToken($token);

        $status = "error"; $code = 200; $data = null; $msg = null;

        if($authCheck){
            $identity = $jwt_auth->checkToken($token, true);

            $user_id = ($identity->sub != null) ? $identity->sub : null;

            if($user_id ) {
                $repository = $this->getDoctrine()->getRepository(Task::class);
                $data = $repository->findBy(array('userid' => $user_id));
            }else{
                $data = array(
                    'status' => 'error',
                    'code'   => 404,
                    'msg'	 => 'Task not found'
                );
            }

            // query for a single Product by its primary key (usually "id")

        }else{
            $data = array(
                'status' => 'error',
                'code'   => 400,
                'msg'	 => 'Authorization not valid'
            );
        }

        return View::create($data, Response::HTTP_OK );
    }

    /**
     * Create a Task.
     * @Rest\Post("/tasks")
     *
     * @return View
     */
    public function saveTask(Request $request,JwtAuth $jwt_auth)
    {
        $em = $this->getDoctrine()->getManager();
        $token = $request->get('authorization',null);

        $authCheck = $jwt_auth->checkToken($token);

        $user_repo = $em->getRepository(User::class);

        $status = "error"; $code = 200; $data = null; $msg = null;

        if($authCheck){
            $identity = $jwt_auth->checkToken($token, true);

            $user_id = ($identity->sub != null) ? $identity->sub : null;
            if($user_id != null){
                // Create Task
                $user = $user_repo->findOneBy(array( "id" => $user_id ));
                if($user) {
                    $task = new Task();
                    $task->setUserid($user->getId());
                    $task->setTitle($request->get('title'));
                    $task->setDate(new \DateTime());
                    $task->setDescription($request->get('description'));
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($task);
                    $em->flush();

                    $status = "success";
                    $data = $task;
                }else{
                    $code = 400;
                    $msg = 'User not found';
                }

            }else{
                $code = 400;
                $msg = 'Task not created, validation failed';
            }

        }else{
            $code = 400;
            $msg = 'Authorization not valid';
        }
        $data = array( "status" => $status, "code" => $code, "data" => $data, "msg"  => $msg );
        return View::create($data, Response::HTTP_CREATED , []);
    }



    /**
     * View Task.
     * @Rest\Post("/tasks/{id}")
     *
     * @return View
     */
    public function viewTask(Request $request,JwtAuth $jwt_auth){
        $id = $request->get('id');

        $token = $request->get('authorization',null);

        $authCheck = $jwt_auth->checkToken($token);

        $status = "error"; $code = 200; $data = null; $msg = null;

        if($authCheck) {
            $repository = $this->getDoctrine()->getRepository(Task::class);

            $identity = $jwt_auth->checkToken($token, true);

            $user_id = ($identity->sub != null) ? $identity->sub : null;

            if($user_id){
                $data = $repository->findOneBy(array('id' => $id,'userid' => $user_id));
                if(is_null($data)){
                    $data = array(
                        'status' => 'error',
                        'code'   => 404,
                        'msg'	 => 'Task not found'
                    );
                }
            }else{
                $data = array(
                    'status' => 'error',
                    'code'   => 404,
                    'msg'	 => 'Task not found'
                );
            }

        }else{
            $data = array(
                'status' => 'error',
                'code'   => 400,
                'msg'	 => 'Authorization not valid'
            );
        }

        return View::create($data,Response::HTTP_OK);
    }

    /**
     * Edit Task.
     * @Rest\Post("/tasks/edit/{id}")
     *
     * @return View
     */
    public function editTask(Request $request,JwtAuth $jwt_auth){
        $id = $request->get('id');

        $token = $request->get('authorization',null);

        $authCheck = $jwt_auth->checkToken($token);

        $status = "error"; $code = 200; $data = null; $msg = null;

        if($authCheck) {
            $repository = $this->getDoctrine()->getRepository(Task::class);

            $identity = $jwt_auth->checkToken($token, true);

            $user_id = ($identity->sub != null) ? $identity->sub : null;

            if($user_id){
                $task = $repository->findOneBy(array('id' => $id,'userid' => $user_id));
                if(is_null($task)){
                    $data = array(
                        'status' => 'error',
                        'code'   => 404,
                        'msg'	 => 'Task not found'
                    );
                }else {
                    if($request->request->has('title')){
                        $task->setTitle($request->get('title'));
                    }
                    $task->setDate(new \DateTime());
                    if($request->request->has('description')){
                        $task->setTitle($request->get('description'));
                    }
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($task);
                    $em->flush();

                    $status = "success";
                    $data = $task;
                }
            }else{
                $data = array(
                    'status' => 'error',
                    'code'   => 404,
                    'msg'	 => 'Task not found'
                );
            }

        }else{
            $data = array(
                'status' => 'error',
                'code'   => 400,
                'msg'	 => 'Authorization not valid'
            );
        }

        return View::create($data,Response::HTTP_OK);
    }
}
