<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\JwtAuth;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
/*use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;*/
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\View\View;
use App\Entity\Task;

use FOS\RestBundle\Controller\Annotations as Rest;
//use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 *
 * @Route("/api")
 */
class UserController extends FOSRestController
{
    /**
     * Lists all Tasks.
     * Rest\Get("/users")
     *
     * @return View
     */
   /* public function getAllUsers():View
    {
        $repository = $this->getDoctrine()->getRepository(User::class);

        // query for a single Product by its primary key (usually "id")
        $user = $repository->findall();

        return View::create($user, Response::HTTP_OK );
    }*/

    /**
     * Create a Task.
     * @Rest\Post("/register")
     *
     * @return View
     */
    public function register(Request $request)
    {
        $user = new User();
        $user->setFirstname($request->get('firstname'));
        $user->setLastname($request->get('lastname'));
        $user->setEmail($request->get('email'));
        $pass = $request->get('password');
        $pwd = hash('sha256', $pass);
        $user->setPassword($pwd);

        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();

        return View::create($user, Response::HTTP_CREATED , []);
    }

    /**
     * View Task.
     * Rest\Get("/users/{id}")
     *
     * @return View
     */
   /* public function viewTask(Request $request){
        $id = $request->get('id');

        $repository = $this->getDoctrine()->getRepository(Task::class);

        $task = $repository->find($id);

        return View::create($task,Response::HTTP_OK);
    }*/

    /**
     * User login.
     * @Rest\Post("/login")
     *
     * @return View
     */
    public function login(Request $request, JwtAuth $jwt_auth){
        $id = $request->get('id');

        $repository = $this->getDoctrine()->getRepository(Task::class);

        $email = $request->get('email');
        $password = $request->get('password');

        $pwd = hash('sha256', $password);
        if($email != null  && $password != null){
            $signup = $jwt_auth->signup($email, $pwd);
            return View::create($signup,Response::HTTP_OK);
        }else{
            $error = array(
                'status' => 'error',
                'data' => 'Email or password incorrect'
            );
        }

        return View::create($error,Response::HTTP_OK);
    }

}
