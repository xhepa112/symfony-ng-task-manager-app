This projects uses symfony 4.0

Please use composer install to install project

Import the database from db folder: the file is database.sql and configure database in .env file 
accordingly to your setup 

use this command to start the server php -S 127.0.0.1:8000 -t public

#Routes 
http://127.0.0.1:8000/api/login - For User Login
http://127.0.0.1:8000/api/register - For User Registration


http://127.0.0.1:8000/api/login - For User Login
http://127.0.0.1:8000/api/register - For User Registration


http://127.0.0.1:8000/api/my-tasks - List tasks of authenticated user
http://127.0.0.1:8000/api/tasks - Create new task
http://127.0.0.1:8000/api/tasks/{id} - View Task
http://127.0.0.1:8000/api/tasks/edit/{id} - Edit Task


Please use POST method for all task controller with the 'authorization' key  and value of token that was 
postbacked when user was authenticated
